package kli18n

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"strings"
)

func pathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func getLprojRes(resPath string, suffix ...string) []string {
	resSuffix := ".lproj"
	if len(suffix) > 0 {
		resSuffix = suffix[0]
	}

	var lprojs []string
	files, _ := ioutil.ReadDir(resPath)

	for _, f := range files {
		if strings.HasSuffix(f.Name(), resSuffix) {
			lprojs = append(lprojs, f.Name())
		}
	}

	return lprojs
}

type i18nModel struct {
	Key     string
	I18nMap map[string]string
}

func readI18nStringsFile(i18nPath string) (i18nMap map[string]string, err error) {
	f, err := os.Open(i18nPath)
	if err != nil {
		return nil, err
	}
	i18nMap = make(map[string]string)
	buf := bufio.NewReader(f)
	for {
		line, _, err := buf.ReadLine()
		if err != nil {
			break
		}
		lineString := strings.TrimSpace(string(line))
		key, content, err := handleI18nLineString(lineString)

		if err != nil {
			// fmt.Println("errpr:", lineString)
		} else {
			i18nMap[key] = content
		}
	}

	return i18nMap, nil
}

func handleI18nLineString(lineString string) (key string, content string, err error) {
	index := strings.Index(lineString, "=")
	if index < 0 {
		err = errors.New("i18n line not containt =")
		return
	}
	arr := strings.SplitN(lineString, "=", 2)
	if len(arr) < 2 {
		return
	} else {
		key = arr[0]
		key = strings.Replace(key, "\"", "", -1)
		key = strings.TrimSpace(key)
		content = strings.TrimSpace(arr[1])
	}

	return
}

func getI18nString(lprojPath string, i18nFilename string) (i18n i18nModel) {
	u, err := url.Parse(lprojPath)
	if err != nil {
		panic(err)
	}

	paths := strings.Split(u.String(), "/")
	lastPath := paths[len(paths)-1]
	filePaths := strings.Split(lastPath, ".")

	i18nFile := lprojPath + "/" + i18nFilename

	i18nMap, err := readI18nStringsFile(i18nFile)

	i18n = i18nModel{}
	i18n.Key = filePaths[0]
	i18n.I18nMap = i18nMap

	return i18n
}

func extract(input string) []string {
	var (
		ret      []string
		matching = false
		buf      []rune
	)
	for _, v := range input {
		switch v {
		case '{':
			if matching {
				buf = nil
			}
			matching = true
		case '}':
			if matching {
				matching = false
				if len(buf) > 0 {
					ret = append(ret, string(buf))
				}
			}
		default:
			if matching {
				buf = append(buf, v)
			}
		}
	}
	return ret
}

func diffCheckI18nMapPlacehold(refMap map[string]string, compMap map[string]string) {
	for key, content := range refMap {
		places := extract(content)
		if len(places) > 0 {
			complaceContent := compMap[key]
			complacePlaces := extract(complaceContent)
			if stringSliceEqual(places, complacePlaces) != true {
				fmt.Println("diff placehold: ", key, places, complacePlaces)
			}
		}
	}
}

func stringSliceEqual(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	if (a == nil) != (b == nil) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func CheckResourcePlaceholderKey(resPath string) {
	// resPath = "/Users/wanqijian/Documents/Klook/kli18n/KLI18n/Resources"
	isExist, err := pathExists(resPath)

	if isExist {

		lprojs := getLprojRes(resPath)

		// lprojs = lprojs[1:3]
		// lprojs = []string{"en.lproj", "ja.lproj"}
		resI18nMap := make(map[string]map[string]string)

		for _, lproj := range lprojs {
			// fmt.Println(lproj)
			lprojPath := resPath + "/" + lproj
			i18nModel := getI18nString(lprojPath, "i18n.strings")
			resI18nMap[i18nModel.Key] = i18nModel.I18nMap
		}

		// en
		refMap := resI18nMap["en"]
		for key, value := range resI18nMap {
			if value == nil {
				continue
			}
			if key == "en" {
				continue
			}

			compMap := value
			// fmt.Println(key, "ok", len(compMap))
			fmt.Println("Compare " + "[en]" + " with [" + key + "]")
			diffCheckI18nMapPlacehold(refMap, compMap)
		}

	} else if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("IsNotExist")
	}

}

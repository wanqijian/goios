package main

import (
	"fmt"
	"os"

	"bitbucket.org/wanqijian/goios/kli18n"
)

func main() {
	args := os.Args
	if len(args) > 1 {

		fmt.Println(args)

		path := args[1]
		kli18n.CheckResourcePlaceholderKey(path)
	}
}
